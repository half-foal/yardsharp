const willardBtn = document.querySelector("#willard-map-btn");
const clevelandBtn = document.querySelector("#cleveland-map-btn");
const clevelandMap = document.querySelector(".cleveland-bg");
const willardMap = document.querySelector(".willard-bg");

willardBtn.addEventListener("click", e => {
  clevelandMap.classList.add("hide-map");
  if (willardMap.classList.contains("hide-map")) {
    willardMap.classList.remove("hide-map");
  }
});

clevelandBtn.addEventListener("click", e => {
  willardMap.classList.add("hide-map");
  if (clevelandMap.classList.contains("hide-map")) {
    clevelandMap.classList.remove("hide-map");
  }
});
