const contentWrapper = document.querySelector(".site-wrapper--content");
const menuBtn = document.querySelector("#menu-btn");
const mobileOverlay = document.querySelector(".mobile-overlay");
const menuBtnPlus = document.querySelector(".menu-btn-plus");
const bodyEl = document.body;

function closeMenu() {
  contentWrapper.classList.remove("menu-opened");
  menuBtnPlus.classList.remove("rotate-plus");
  bodyEl.classList.remove("scroll-lock");
}
function openMenu() {
  contentWrapper.classList.add("menu-opened");
  menuBtnPlus.classList.add("rotate-plus");
  bodyEl.classList.add("scroll-lock");
}
menuBtn.addEventListener("click", e => {
  e.preventDefault();
  if (contentWrapper.classList.contains("menu-opened")) {
    closeMenu();
  } else {
    openMenu();
  }
});

mobileOverlay.addEventListener("click", e => {
  e.preventDefault();
  if (contentWrapper.classList.contains("menu-opened")) {
    closeMenu();
  }
});
